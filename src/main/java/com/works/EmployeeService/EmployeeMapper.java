package com.works.EmployeeService;

import com.works.EmployeeService.model.Employee;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

public class EmployeeMapper {
    public static void mappingEmployeeDetails(@RequestBody @Valid Employee employeeDetails, Employee employee) {
        employee.setDate_of_birth(employeeDetails.getDate_of_birth());
        employee.setFirst_name(employeeDetails.getFirst_name());
        employee.setLast_name(employeeDetails.getLast_name());
        employee.setAddress(employeeDetails.getAddress());
        employee.setEmail(employeeDetails.getEmail());
        employee.setSalary(employeeDetails.getSalary());
    }
}
