package com.works.EmployeeService.repository;

import com.works.EmployeeService.model.EmployeePost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeePostRepository extends JpaRepository<EmployeePost,Integer> {
}
