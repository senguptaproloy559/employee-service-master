package com.works.EmployeeService.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Tutorial {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Size(min = 2, message = "title should be atlest 2 character")
    private  String title;

    @NotNull
    @Size(min = 2, message = "decription should be atlest 2 character")
    private  String description;
    @NotNull
    //@Size(min = 2, message = "published should be atlest 2 character")
    private  boolean published;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }
    public  Tutorial(){}

    public Tutorial(@NotNull @Size(min = 2, message = "title should be atlest 2 character") String title, @NotNull @Size(min = 2, message = "decription should be atlest 2 character") String description, @NotNull @Size(min = 2, message = "published should be atlest 2 character") boolean published) {
        //this.id = id;
        this.title = title;
        this.description = description;
        this.published = published;
    }

    @Override
    public String toString() {
        return "Tutorial{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", published=" + published +
                '}';
    }
}
