package com.works.EmployeeService.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.*;

@Entity
public class Employee {
    @Id
    @GeneratedValue
    private Integer id;

    @NotNull
    @Size(min = 2, message = "First Name should be atlest 2 character")
    private String first_name;

    @NotNull
    @Size(min = 2, message = "Last Name should be atlest 2 character")
    private String last_name;

    @NotNull
    @Past( message = "Date Of Birth cannot ne present date")
    private Date date_of_birth;

    @NotNull
    @Email(message = "Please provide valid Email ID")
    private String email;

    @NotNull
    @Size(min = 2, message = "Address should be 5 character")
    private String address;

    @NotNull
    @DecimalMax(value = "7000")
    private int salary;

    @OneToMany(mappedBy = "employee")
    //@JsonIgnore
    private List<EmployeePost> employeePost;

    public List<EmployeePost> getEmployeePost() {
        return employeePost;
    }

    public void setEmployeePost(List<EmployeePost> employeePost) {
        this.employeePost = employeePost;
    }

    public Employee() {	}

    public Employee(Integer id,String first_name, String last_name, Date date_of_birth,String email, String address, int salary) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.date_of_birth = date_of_birth;
        this.email=email;
        this.address = address;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", date_of_birth=" + date_of_birth +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", salary=" + salary +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public Date getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }


}