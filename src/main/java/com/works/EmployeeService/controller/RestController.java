package com.works.EmployeeService.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import com.works.EmployeeService.HelloWorldBean;
import com.works.EmployeeService.exception.EmployeeNotFoundException;
import com.works.EmployeeService.model.Employee;
import com.works.EmployeeService.service.EmployeeDaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


@org.springframework.web.bind.annotation.RestController
public class RestController {

    @Autowired
    private EmployeeDaoService employeeDaoService;

    @RequestMapping(path = "/hello-world", method = RequestMethod.GET)
    public String getHelloWorld() {
        return "HelloWorld";
    }

    @RequestMapping(path = "/hello-world-bean", method = RequestMethod.GET)
    public HelloWorldBean getHelloWorldBean() {
        return new HelloWorldBean("HelloWorld");
    }

    @RequestMapping(path = "/hello-world-path-variables/{name}")
    public HelloWorldBean getGelloWorldWorldPathVariable(@PathVariable String name) {
        return new HelloWorldBean("Welcome! Hello World" + name);
    }

    //RetreiverAllUsers
    @RequestMapping(path = "/users", method = RequestMethod.GET)
    public List<Employee> getAllDAta() {
        return employeeDaoService.getAllDAta();
    }

    // Retreive UserBy Id

    @RequestMapping(path = "/employees/{id}", method = RequestMethod.GET)
    public Employee getUserById(@PathVariable int id) {
        Employee employee = employeeDaoService.getID(id);
        if (employee == null)
            throw new EmployeeNotFoundException("id is ::" + id);

        return employee;
    }


    //Save application
    @RequestMapping(path = "/employees/save", method = RequestMethod.POST)
    public ResponseEntity<Object> saveEmployee(@Valid @RequestBody Employee employee) {
        Employee EmployeesDAta = employeeDaoService.save(employee);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(EmployeesDAta.getId())
                .toUri();
        return ResponseEntity.created(location).build();

    }

    // Delete Appliction
    @RequestMapping(path = "/employees/{id}", method = RequestMethod.DELETE)
    public Employee deleteEmployeeById(@PathVariable int id) {
        Employee Employee = employeeDaoService.deleteById(id);
        if (Employee == null)
            throw new EmployeeNotFoundException("id is ::" + id);
        return Employee;
    }
}
