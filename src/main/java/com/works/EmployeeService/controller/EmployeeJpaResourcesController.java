package com.works.EmployeeService.controller;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import com.works.EmployeeService.EmployeeMapper;
import com.works.EmployeeService.exception.EmployeeNotFoundException;
import com.works.EmployeeService.model.Employee;
import com.works.EmployeeService.model.EmployeePost;
import com.works.EmployeeService.repository.EmployeePostRepository;
import com.works.EmployeeService.repository.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


@RestController
public class EmployeeJpaResourcesController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private EmployeePostRepository employeePostRepository;

	private EmployeeMapper employeeMapper;

	@RequestMapping(path = "/jpa/employees", method = RequestMethod.GET)
	public List<Employee> getEmployeeAllDAta() {
		logger.info("Getting employee details from the database.");
		return employeeRepository.findAll();
	}


	// Retreive EmployeeBy Id

	@GetMapping("/jpa/employees/{id}")
	public Employee getEmployeeById(@PathVariable int id) {
		logger.info("Getting employee details w.r.t id");
		Optional<Employee> employee = employeeRepository.findById(id);
		if (!employee.isPresent())
			throw new EmployeeNotFoundException("This ID is not Exist  ::" + id);
			return employee.get();

	}

	@RequestMapping(path = "/jpa/employees/save", method = RequestMethod.POST)
	public ResponseEntity<Object> saveEmployee(@Valid @RequestBody Employee employee) {
		logger.info("Saving employee  details in the database.");
		Employee employeesDAta = employeeRepository.save(employee);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(employeesDAta.getId())
				.toUri();
		return ResponseEntity.created(location).build();

	}

	@PutMapping("/jpa/update/employees/{id}")
	public ResponseEntity <Employee> updateEmployee(@PathVariable(value = "id") int id,
													  @Valid @RequestBody Employee employeeDetails) {
		logger.info("Updating  employee details in the database.");
		Employee employee = employeeRepository.findById(id).orElseThrow(() -> new EmployeeNotFoundException("Employee Not found for this id ::"+id));
		EmployeeMapper.mappingEmployeeDetails(employeeDetails,employee);
		final Employee updatedEmployee=employeeRepository.save(employee);
		return  ResponseEntity.ok(updatedEmployee);
	}


	@DeleteMapping("/jpa/employees/delete/{id}")
	public Map < String, Boolean > getDeleteById(@PathVariable(value = "id") int id) {
		logger.info("Deleting  employee details from the database. w.r.t "+id);
		Employee employee = employeeRepository.findById(id).orElseThrow(() -> new EmployeeNotFoundException("Employee Not found for this id ::"+id));

		employeeRepository.delete(employee);

		Map<String,Boolean>message=new HashMap<>();
		message.put("deleted",Boolean.TRUE);
		return message;

}

	@RequestMapping(path = "/jpa/employees/{id}/savePost", method = RequestMethod.POST)
	public ResponseEntity<Object> saveEmployeesPost(@Valid @PathVariable(value = "id") int id,
													@RequestBody EmployeePost employeePost) {


		Optional<Employee> employeeOptional = employeeRepository.findById(id);

		if (!employeeOptional.isPresent())
			throw new EmployeeNotFoundException("This ID is not Exist  ::" + id);

		Employee employeeData = employeeOptional.get();

		employeePost.setEmployee(employeeData);

		 employeePostRepository.save(employeePost);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(employeePost.getId())
				.toUri();
		return ResponseEntity.created(location).build();

	}


	@GetMapping("/jpa/employees/{id}/employeePosts")
	public List<EmployeePost> getEmployeePosts(@PathVariable(value = "id") int id) {
		Optional<Employee> employee = employeeRepository.findById(id);
		if (!employee.isPresent())
			throw new EmployeeNotFoundException("This ID is not Exist  ::" + id);
		return employee.get().getEmployeePost();

	}

}
