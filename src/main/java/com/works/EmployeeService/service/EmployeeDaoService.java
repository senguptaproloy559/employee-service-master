package com.works.EmployeeService.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.works.EmployeeService.model.Employee;
import org.springframework.stereotype.Component;

@Component
public class EmployeeDaoService {
	private static List<Employee> listofEmployees = new ArrayList<>();
	private static int EmployeeCount = 5;
	static {
		listofEmployees.add(new Employee(1, "Tuhin", "Mandal",new Date(),"abc@bc.com","ABC",10));
		listofEmployees.add(new Employee(2, "Subhra", "Mandal",new Date(),"abc@bc.com","ABCD",10));
		listofEmployees.add(new Employee(3, "Subrata", "Mandal",new Date(),"abc@bc.com","ABCDE",10));
		listofEmployees.add(new Employee(4, "Papu","Mandal",new Date(),"ABCDEF","abc@bc.com",10));
		listofEmployees.add(new Employee(5, "Gogonta", "Mandal",new Date(),"ABCDEFG","abc@bc.com",10));
	}

	public List<Employee> getAllDAta() {
		return listofEmployees;
	}

	public Employee save(Employee Employee) {
		if (Employee.getId() == null) {
			Employee.setId(++EmployeeCount);
		}
		listofEmployees.add(Employee);
		return Employee;
	}

	public Employee getID(int id) {
		for (Employee Employee : listofEmployees) {
			if (Employee.getId() == id) {
				return Employee;
			}
			// return null;
		}
		return null;

	}
	
	public Employee deleteById(int id) {
		Iterator<Employee>itr=listofEmployees.iterator();
		while(itr.hasNext()) {
			Employee Employee=itr.next();
			if(Employee.getId() == id) {
				itr.remove();
				return Employee;
			}
		}
		return null;
		
	}
}
